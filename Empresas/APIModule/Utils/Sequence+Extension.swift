//
//  Sequence+Extension.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 11/07/21.
//

import Foundation

extension Sequence where Element : Hashable {
    
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
