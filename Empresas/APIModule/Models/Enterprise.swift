//
//  Enterprise.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 11/07/21.
//

import Foundation

public class Enterprise : Codable, Identifiable {
 
    public var id : Int
    public var name : String?
    public var description : String?
    public var email : String?
    public var facebook : String?
    public var twitter : String?
    public var linkedin : String?
    public var phone : String?
    public var ownEnterprise : Bool = false
    public var photo : String?
    public var value : Decimal = 0
    public var shares : Decimal = 0
    public var sharePrice : Decimal = 0
    public var ownShares : Decimal = 0
    public var city : String?
    public var country : String?
    public var enterpriseType : EnterpriseType?
    
    public var sharePriceString : String? {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        nf.locale = Locale(identifier: "pt_BR")
        return nf.string(from: sharePrice as NSNumber)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_name"
        case description
        case email = "email_enterprise"
        case facebook
        case twitter
        case linkedin
        case phone
        case ownEnterprise = "own_enterprise"
        case photo
        case value
        case shares
        case sharePrice = "share_price"
        case ownShares = "own_shares"
        case city
        case country
        case enterpriseType = "enterprise_type"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? UUID().hashValue
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.email = try container.decodeIfPresent(String.self, forKey: .email)
        self.facebook = try container.decodeIfPresent(String.self, forKey: .facebook)
        self.twitter = try container.decodeIfPresent(String.self, forKey: .twitter)
        self.linkedin = try container.decodeIfPresent(String.self, forKey: .linkedin)
        self.phone = try container.decodeIfPresent(String.self, forKey: .phone)
        self.ownEnterprise = try container.decodeIfPresent(Bool.self, forKey: .ownEnterprise) ?? false
        if let photoPath = try container.decodeIfPresent(String.self, forKey: .photo) {
            self.photo = APIBase.BASE_PATH + photoPath
        }
        self.value = try container.decodeIfPresent(Decimal.self, forKey: .value) ?? 0
        self.shares = try container.decodeIfPresent(Decimal.self, forKey: .shares) ?? 0
        self.sharePrice = try container.decodeIfPresent(Decimal.self, forKey: .sharePrice) ?? 0
        self.ownShares = try container.decodeIfPresent(Decimal.self, forKey: .ownShares) ?? 0
        self.city = try container.decodeIfPresent(String.self, forKey: .city)
        self.country = try container.decodeIfPresent(String.self, forKey: .country)
        self.enterpriseType = try container.decodeIfPresent(EnterpriseType.self, forKey: .enterpriseType)
    }
}
