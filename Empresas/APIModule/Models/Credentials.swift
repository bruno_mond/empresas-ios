//
//  Credentials.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import Foundation

public class Credentials : Codable {
    
    public var email : String = ""
    public var password : String = ""
    
    public init() {
        
    }
    
}
