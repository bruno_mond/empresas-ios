//
//  AuthenticationData.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import Foundation

public class AuthenticationData : Codable {
    
    var accessToken : String?
    var client : String?
    var uid : String?
    
    init() {
        
    }
    
    init(with response : HTTPURLResponse) {
        accessToken = response.allHeaderFields["access-token"] as? String
        client = response.allHeaderFields["client"] as? String
        uid = response.allHeaderFields["uid"] as? String
    }
}
