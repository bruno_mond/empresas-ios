//
//  EnterpriseType.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 11/07/21.
//

import Foundation

public class EnterpriseType : Codable, Hashable {
    
    public static func == (lhs: EnterpriseType, rhs: EnterpriseType) -> Bool {
        if lhs.id == rhs.id {
            return true
        } else {
            return false
        }
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
        }
    
    public var id : Int
    public var name : String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_type_name"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? UUID().hashValue
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
    }
}
