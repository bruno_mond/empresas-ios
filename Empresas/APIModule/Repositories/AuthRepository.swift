//
//  AuthRepository.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import Foundation
import SwiftUI

public class AuthRepository : ObservableObject {
    
    public static let shared : AuthRepository = AuthRepository()
    
    @AppStorage("authEntity") private var entityData : Data?
    
    public var entity : AuthenticationData? {
        get {
            guard let data = entityData else { return nil }
            let decoder = JSONDecoder()
            if let entity = try? decoder.decode(AuthenticationData.self, from: data) {
                return entity
            }
            
            return nil
        }
        
        set {
            let encoder = JSONEncoder()
            if let data = try? encoder.encode(newValue) {
                DispatchQueue.main.async {
                    self.entityData = data
                }
            }
        }
    }
    
    public func login(credential: Credentials, completion: @escaping (_ success: () throws -> Void) -> Void) {
        LoginServices.shared.signIn(credential: credential) { result in
            
            switch result {
            
            case .success(let authEntity):
                self.entity = authEntity
                completion({ return })
                
            case .failure(let error):
                completion({throw error})
            }
        }
    }
}
