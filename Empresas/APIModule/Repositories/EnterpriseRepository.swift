//
//  EnterpriseRepository.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 11/07/21.
//

import SwiftUI

public class EnterpriseRepository : ObservableObject {
    
    public static let shared : EnterpriseRepository = EnterpriseRepository()
    
    @Published public var enterprises : [Enterprise] = []
    @Published public var enterpriseTypes : [EnterpriseType] = []
    
    public func search(typeIndex : Int?, searchString : String?, completion: @escaping (_ success: () throws -> Void) -> Void) {
        
        SearchServices.shared.search(typeIndex: typeIndex, searchString: searchString) { result in
            
            switch result {
            
            case .success(let tupple):
                DispatchQueue.main.async {
                    self.enterprises = tupple.enterprises
                    self.enterpriseTypes = tupple.types
                    completion({ return })
                }
                
            case .failure(let error):
                completion({ throw error })
                
            }
        }
        
    }
    
    public func search(enterpriseId : Int, completion: @escaping (_ getEnterprise: () throws -> Enterprise) -> Void) {
        SearchServices.shared.search(enterpriseId: enterpriseId) { result in
            switch result {
            
            case .success(let enterprise):
                completion({ return enterprise })
                
            case .failure(let error):
                completion({ throw error })
                
            }
        }
    }
    
}
