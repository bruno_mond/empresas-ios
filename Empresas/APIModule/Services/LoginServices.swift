//
//  LoginServices.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import Foundation

public class LoginServices : APIBase, HTTPLoginProtocol {
    
    public static let shared : LoginServices = LoginServices()
    
    public func signIn(credential : Credentials, completion : @escaping (Result<AuthenticationData, LoginAPIError>) -> Void) {
        
        let path = "\(APIBase.BASE_PATH)\(APIBase.API_VERSION)/users/auth/sign_in"
        guard let resourceUrl = URL(string: path) else {
            completion(.failure(.InvalidUrl))
            return
        }
        
        var urlRequest = URLRequest(url: resourceUrl)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = try! JSONEncoder().encode(credential)
        
        let signInTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            if error != nil {
                completion(.failure(.ServerError(errorDescription: error!.localizedDescription)))
                return
            }
            
            guard let responseData = response as? HTTPURLResponse, let jsonData = data else {
                completion(.failure(.DataNotFound))
                return
            }
            
            if responseData.statusCode == 200 {
                let authEntity = AuthenticationData(with: responseData)
                completion(.success(authEntity))
                
            } else if responseData.statusCode == 401 {
                completion(.failure(.Unauthorized))

            } else {
                if let errorDescription = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let errors = errorDescription["errors"] as? [String], errors.count > 0 {
                    completion(.failure(.ServerError(errorDescription: errors[0])))
                } else {
                    completion(.failure(.ServerError(errorDescription: "Erro no login")))
                }
            }
            
        }
        signInTask.resume()
        
    }
    
    public enum LoginAPIError : LocalizedError, Identifiable {

        case DataNotFound
        case Unauthorized
        case InvalidUrl
        case ServerError(errorDescription: String?)
        
        public var id: String {
            self.localizedDescription
        }
        
        public var errorDescription: String? {
            switch self {
            case .DataNotFound: return "Dados não encontrados"//NSLocalizedString("Alerts_DataNotFound", comment: "Data not found, try again later!")
            case .Unauthorized: return "Credenciais incorretas"//NSLocalizedString("Login_UnauthorizedDescription", comment: "Invalid login credentials, try again later!")
            case .InvalidUrl : return "Erro no login"//NSLocalizedString("Alerts_InvalidUrl", comment: "Error, invalid url!")
            case .ServerError(let errorDescription): return errorDescription
            }
        }
    }
}
