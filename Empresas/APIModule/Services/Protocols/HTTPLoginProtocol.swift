//
//  HTTPLoginProtocol.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 12/07/21.
//

import Foundation

protocol HTTPLoginProtocol {
    
    func signIn(credential : Credentials, completion : @escaping (Result<AuthenticationData, LoginServices.LoginAPIError>) -> Void)
}
