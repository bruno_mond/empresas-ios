//
//  APIBase.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import Foundation

public class APIBase {
    
    static let BASE_PATH : String = "https://empresas.ioasys.com.br"
    static let API_VERSION : String = "/api/v1"
    
}
