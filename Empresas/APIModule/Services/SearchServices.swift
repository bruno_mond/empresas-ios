//
//  SearchServices.swift
//  APIModule
//
//  Created by Bruno Monteiro de Andrade on 11/07/21.
//

import Foundation

public class SearchServices : APIBase {
    
    public static let shared : SearchServices = SearchServices()
    let authRepository : AuthRepository = AuthRepository.shared
    
    public func search(typeIndex : Int?, searchString : String?, completion : @escaping (Result<(enterprises : [Enterprise], types : [EnterpriseType]), SearchAPIError>) -> Void) {
        
        var path = "\(APIBase.BASE_PATH)\(APIBase.API_VERSION)/enterprises"
        
        if typeIndex != nil && searchString != nil {
            path += "?enterprise_types=\(typeIndex!)&name=\(searchString!)"
            
        } else if typeIndex != nil {
            path += "?enterprise_types=\(typeIndex!)"
            
        } else if searchString != nil {
            path += "?name=\(searchString!)"
            
        }
        guard let resourceUrl = URL(string: path), let authHeaderInfo = authRepository.entity,
              let accessToken = authHeaderInfo.accessToken, let client = authHeaderInfo.client,
              let uid = authHeaderInfo.uid else {
            completion(.failure(.Unauthorized))
            return
        }
        
        var urlRequest = URLRequest(url: resourceUrl)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(accessToken, forHTTPHeaderField: "access-token")
        urlRequest.addValue(client, forHTTPHeaderField: "client")
        urlRequest.addValue(uid, forHTTPHeaderField: "uid")
        
        let searchTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            if error != nil {
                completion(.failure(.ServerError(errorDescription: error!.localizedDescription)))
                return
            }
            
            guard let responseData = response as? HTTPURLResponse, let jsonData = data else {
                completion(.failure(.DataNotFound))
                return
            }
            
            if responseData.statusCode == 200 {
                if let enterprisesJsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let enterprisesData = try? JSONSerialization.data(withJSONObject: enterprisesJsonObject["enterprises"] ?? [], options: .fragmentsAllowed) {
                    
                    if let enterprises = try? JSONDecoder().decode([Enterprise].self, from: enterprisesData) {
                        let types : [EnterpriseType] = enterprises.compactMap({ $0.enterpriseType }).uniqued()
                        completion(.success((enterprises : enterprises, types : types)))
                    }  else {
                        completion(.failure(.DataNotFound))
                    }
                }
                
            } else if responseData.statusCode == 401 {
                completion(.failure(.Unauthorized))

            } else {
                if let errorDescription = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let errors = errorDescription["errors"] as? [String], errors.count > 0 {
                    completion(.failure(.ServerError(errorDescription: errors[0])))
                } else {
                    completion(.failure(.ServerError(errorDescription: NSLocalizedString("Alerts_AnErrorOcurred", comment: "An error ocurred, try again later!"))))
                }
            }
            
        }
        searchTask.resume()
        
    }
    
    
    public func search(enterpriseId : Int, completion : @escaping (Result<Enterprise, SearchAPIError>) -> Void) {
        
        let path = "\(APIBase.BASE_PATH)\(APIBase.API_VERSION)/enterprises/\(enterpriseId)"
        
        guard let resourceUrl = URL(string: path), let authHeaderInfo = authRepository.entity,
              let accessToken = authHeaderInfo.accessToken, let client = authHeaderInfo.client,
              let uid = authHeaderInfo.uid else {
            completion(.failure(.Unauthorized))
            return
        }
        
        var urlRequest = URLRequest(url: resourceUrl)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(accessToken, forHTTPHeaderField: "access-token")
        urlRequest.addValue(client, forHTTPHeaderField: "client")
        urlRequest.addValue(uid, forHTTPHeaderField: "uid")
        
        let searchTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            if error != nil {
                completion(.failure(.ServerError(errorDescription: error!.localizedDescription)))
                return
            }
            
            guard let responseData = response as? HTTPURLResponse, let jsonData = data else {
                completion(.failure(.DataNotFound))
                return
            }
            
            if responseData.statusCode == 200 {
                if let enterprisesJsonObject = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let enterprisesData = try? JSONSerialization.data(withJSONObject: enterprisesJsonObject["enterprise"] as Any, options: .fragmentsAllowed) {
                    
                    if let enterprise = try? JSONDecoder().decode(Enterprise.self, from: enterprisesData) {
                        completion(.success(enterprise))
                        
                    } else {
                        completion(.failure(.DataNotFound))
                        
                    }
                }
                
            } else if responseData.statusCode == 401 {
                completion(.failure(.Unauthorized))

            } else {
                if let errorDescription = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String : Any], let errors = errorDescription["errors"] as? [String], errors.count > 0 {
                    completion(.failure(.ServerError(errorDescription: errors[0])))
                    
                } else {
                    completion(.failure(.ServerError(errorDescription: NSLocalizedString("Alerts_AnErrorOcurred", comment: "An error ocurred, try again later!"))))
                    
                }
            }
            
        }
        searchTask.resume()
        
    }
    
    public enum SearchAPIError : LocalizedError, Identifiable {

        case DataNotFound
        case Unauthorized
        case InvalidUrl
        case ServerError(errorDescription: String?)
        
        public var id: String {
            self.localizedDescription
        }
        
        public var errorDescription: String? {
            switch self {
            case .DataNotFound: return NSLocalizedString("Alerts_DataNotFound", comment: "Data not found, try again later!")
            case .Unauthorized: return NSLocalizedString("Enterprises_UnauthorizedDescription", comment: "Your login expired, login again please!")
            case .InvalidUrl : return NSLocalizedString("Alerts_InvalidUrl", comment: "Error, invalid url!")
            case .ServerError(let errorDescription): return errorDescription
            }
        }
    }
}
