//
//  LoginViewModel.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import SwiftUI
import APIModule

class LoginViewModel : ObservableObject {
    
    @ObservedObject var authRepository : AuthRepository = AuthRepository.shared
    
    @Published var credentials : Credentials = Credentials()
    @Published var loading : Bool = false
    @Published var isKeyboardShowing : Bool = false
    
    var error : LoginServices.LoginAPIError?
    
    init() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main) { _ in
            self.isKeyboardShowing = true
        }
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { _ in
            self.isKeyboardShowing = false
        }
    }
    
    var hasError : Bool {
        get {
            error != nil
        }
        set {
            if !newValue {
                self.error = nil
            }
        }
    }
    
    func login() {
        
        loading = true
        
        authRepository.login(credential: credentials) { success in
            DispatchQueue.main.async {
                self.loading = false
            }
            
            do {
                try success()
                
            } catch let error {
                guard let err = error as? LoginServices.LoginAPIError else { print(error.localizedDescription); return }
                self.error = err
                
            }
        }
    }
}
