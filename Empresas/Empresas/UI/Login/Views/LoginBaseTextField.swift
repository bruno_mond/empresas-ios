//
//  LoginBaseTextField.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 19/07/21.
//

import SwiftUI

struct LoginBaseTextField: View {
    
    var labelText : String
    @Binding var fieldValue : String
    @Binding var hasError : Bool
    var keyboardType : UIKeyboardType = .default
    
    @State var isSelected : Bool = false {
        didSet {
            if isSelected && hasError {
                hasError = false
            }
        }
    }
    
    var isPassword : Bool = false
    @State var isVisible : Bool = false
    @State var localPassword : String = ""
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 4) {
            
            Text(labelText)
                .font(.custom("Rubik-Regular", size: 14))
                .foregroundColor(isSelected ? Color("PrimaryColor") : Color("LabelColor"))
                .padding(.horizontal, 4)
            
            ZStack(alignment: .trailing) {
                
                if !isVisible && isPassword {
                    
                    SecureField(fieldValue, text: $fieldValue, onCommit: {
                        self.isSelected = false
                    })
                    .onTapGesture {
                        self.isSelected = true
                        self.fieldValue = $fieldValue.wrappedValue
                    }
                    .font(.custom("Rubik-Thin", size: 16))
                    .padding()
                    .padding(.trailing, isPassword ? 45 : 0)
                    .background(RoundedRectangle(cornerRadius: 4).fill(Color("TextFieldColor")))
                    .foregroundColor(.black)
                    .frame(height: 48)
                    .overlay(
                            RoundedRectangle(cornerRadius: 4)
                                .stroke(Color.red, lineWidth: hasError ? 1 : 0)
                        )
                    .onChange(of: fieldValue) {
                        if fieldValue.count == 1 && localPassword.count > 1 {
                            fieldValue = localPassword + $0
                        }
                        localPassword = $0
                    }
                    
                } else {
                    
                    TextField(fieldValue, text: $fieldValue, onEditingChanged: {
                        self.isSelected = $0
                    })
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .keyboardType(keyboardType)
                    .font(.custom("Rubik-Thin", size: 16))
                    .padding()
                    .padding(.trailing, isPassword ? 45 : 0)
                    .background(RoundedRectangle(cornerRadius: 4).fill(Color("TextFieldColor")))
                    .foregroundColor(.black)
                    .frame(height: 48)
                    .overlay(
                            RoundedRectangle(cornerRadius: 4)
                                .stroke(Color.red, lineWidth: hasError ? 1 : 0)
                        )
                    .onChange(of: fieldValue) {
                        if isPassword {
                            localPassword = $0
                        }
                    }
                    
                }
                
                if isPassword && !hasError {
                    Button(action: {
                        isVisible.toggle()
                    }) {
                        Image(systemName: self.isVisible ? "eye.slash.fill" : "eye.fill")
                            .accentColor(Color("LabelColor"))
                            .padding(.trailing, 14)
                    }
                } else if hasError {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(.red)
                        .padding(.trailing, 14)
                }
                
            }
            
        }
        
    }
}

struct LoginBaseTextField_Previews: PreviewProvider {
    static var previews: some View {
        LoginBaseTextField(labelText: "Email", fieldValue: .constant("teste@teste.com"), hasError: .constant(false))
    }
}
