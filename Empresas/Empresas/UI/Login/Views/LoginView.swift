//
//  LoginView.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import SwiftUI
import APIModule

struct LoginView: View {
    
    @StateObject var loginViewModel : LoginViewModel = LoginViewModel()
    
    var body: some View {
        
        GeometryReader { geometry in
            
            ZStack {
                
                VStack(alignment: .leading, spacing: 25) {
                    
                    
                    header
                        .frame(width: geometry.size.width, height: loginViewModel.isKeyboardShowing ? 150 : 250)
                    
                    
                    form
                        .disabled(loginViewModel.loading)
                    
                    Spacer()
                }
                
                if loginViewModel.loading {
                    VStack {
                        Spacer()
                        
                        LoadingView()
                        
                        Spacer()
                    }
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    .background(Color.black.opacity(0.3))
                }
                
                
            }
            
        }
        .ignoresSafeArea(.all, edges: .all)
    }
    
    var header : some View {
        VStack(spacing: 16) {
            
            Spacer()
            
            Image("IoasysLogoSmall")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width:40, height: 40)
                .padding(.top, loginViewModel.isKeyboardShowing ? 40 : 0)
            
            if !$loginViewModel.isKeyboardShowing.animation(.easeInOut).wrappedValue {
                Text("Seja bem-vindo ao empresas!")
                    .font(.custom("Rubik-Regular", size: 20))
                    .foregroundColor(.white)
            }
                
            
            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Image("LoginTopBarBackground")
                        .resizable()
                        .scaledToFill()
                        .clipShape(BottomRoundedRectangle())
                        .clipped())
    }
    
    var form : some View {
        VStack(spacing: 40) {
            
            VStack(spacing: 16) {
                
                LoginBaseTextField(labelText: "Email", fieldValue: $loginViewModel.credentials.email, hasError: $loginViewModel.hasError, keyboardType: .emailAddress)
                
                VStack(alignment: .trailing, spacing: 4) {
                    LoginBaseTextField(labelText: "Senha", fieldValue: $loginViewModel.credentials.password, hasError: $loginViewModel.hasError, isPassword: true)
                    
                    if loginViewModel.hasError {
                        if let error = loginViewModel.error {
                            Text(error.localizedDescription)
                                .font(.custom("Rubik-Thin", size: 13))
                                .foregroundColor(.red)
                        }
                        
                    }
                }
                
                
            }
            .padding(.horizontal, 16)
            
            Button("ENTRAR") {
                loginViewModel.login()
            }
            .buttonStyle(BaseButton(color: Color("PrimaryColor")))
            .padding(.horizontal, 30)
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

