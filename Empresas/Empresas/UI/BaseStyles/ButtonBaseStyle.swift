//
//  ButtonBaseStyle.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import SwiftUI

struct BaseButton: ButtonStyle {
    
    let color : Color
    
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            configuration.label
                .padding(.vertical)
                .padding(.horizontal)
                .foregroundColor(.white)
                .font(.bold(.headline)())
                
        }
        .frame(maxWidth: .infinity)
        .frame(height : 48)
        .background(color)
        .cornerRadius(8)
        .overlay(
            Color.black
                .opacity(configuration.isPressed ? 0.3 : 0)
                .clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous))
        )    
    }
}
