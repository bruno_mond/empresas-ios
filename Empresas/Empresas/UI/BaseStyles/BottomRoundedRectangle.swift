//
//  BottomRoundedRectangle.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 19/07/21.
//

import SwiftUI

struct BottomRoundedRectangle: Shape {
    
    func path(in rect: CGRect) -> Path {
        
        var path = Path()
        
        let topLeft = CGPoint(x: rect.minX, y: rect.minY)
        path.move(to: topLeft)
        
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY * 0.75))
        
        path.addQuadCurve(to: CGPoint(x: rect.maxX, y: rect.maxY * 0.75), control: CGPoint(x: rect.midX, y: rect.maxY))
        
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        
        path.closeSubpath()
        
        return path
    }
}
