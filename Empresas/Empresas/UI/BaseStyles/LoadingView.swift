//
//  LoadingView.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 19/07/21.
//

import SwiftUI

struct LoadingView: View {
    @State var isRotating : Bool = false
    
    var body: some View {
        ZStack {
            Circle()
                .trim(from: 0, to: 3/4)
                .rotation(.degrees(270))
                .stroke(lineWidth: 3)
                .frame(width: 72, height: 72, alignment: .center)
                .foregroundColor(Color("SecondaryColor"))
                .rotationEffect(.degrees(isRotating ? 360 : 0))
            
            Circle()
                .trim(from: 0, to: 3/4)
                .rotation(.degrees(90))
                .stroke(lineWidth: 3)
                .frame(width: 47, height: 47, alignment: .center)
                .foregroundColor(Color("SecondaryColor"))
                .rotationEffect(.degrees(isRotating ? -360 : 0))
        }
        .onAppear {
            
            withAnimation(Animation.linear(duration: 1).repeatForever(autoreverses: false)) {
                self.isRotating.toggle()
            }
                
            
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
