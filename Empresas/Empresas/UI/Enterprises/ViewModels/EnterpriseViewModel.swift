//
//  EnterpriseViewModel.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 11/07/21.
//

import SwiftUI
import APIModule

class EnterpriseViewModel : ObservableObject {
    
    @ObservedObject var enterpriseRepository : EnterpriseRepository = EnterpriseRepository.shared
    @ObservedObject var authRepository : AuthRepository = AuthRepository.shared
    
    @Published var enterprises : [Enterprise] = []
    @Published var enterpriseTypes : [EnterpriseType] = []
    @Published var loading : Bool = false
    @Published var selectedEnterprise : Enterprise?
    
    @Published var searchString : String = ""
    @Published var selectedTypeId : Int?
    
    @Published var isKeyboardShowing : Bool = false
    @Published var hasSearch : Bool = false
    
    init() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: .main) { _ in
            self.isKeyboardShowing = true
        }
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: .main) { _ in
            self.isKeyboardShowing = false
        }
    }
    
    var error : SearchServices.SearchAPIError?
    
    var hasError : Bool {
        error != nil
    }
    
    func search() {
        loading = true
        
        enterpriseRepository.search(typeIndex: selectedTypeId, searchString: searchString == "" ? nil : searchString) { success in
            DispatchQueue.main.async {
                self.loading = false
                self.hasSearch = true
            }
            
            do {
                try success()
                self.enterprises = self.enterpriseRepository.enterprises
                self.enterpriseTypes = self.enterpriseRepository.enterpriseTypes
                
            } catch let error {
                guard let err = error as? SearchServices.SearchAPIError else { print(error.localizedDescription); return }
                self.error = err
            }
            
        }
    }
    
    func searchById(enterpriseId : Int) {
        loading = true
        
        enterpriseRepository.search(enterpriseId : enterpriseId) { getEnterprise in
            
            DispatchQueue.main.async {
                self.loading = false
            }
            
            do {
                let enterprise = try getEnterprise()
                DispatchQueue.main.async {
                    self.selectedEnterprise = enterprise
                }
            }  catch let error {
                guard let err = error as? SearchServices.SearchAPIError else { print(error.localizedDescription); return }
                self.error = err
            }
        }
    }
    
    func logout() {
        authRepository.entity = nil
    }
}

