//
//  EnterpriseDetailView.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 12/07/21.
//

import SwiftUI
import APIModule
import URLImage

struct EnterpriseDetailView: View {
    
    @ObservedObject var enterpriseViewModel : EnterpriseViewModel
    var enterprise : Enterprise
    
    @Environment(\.presentationMode) private var presentationMode
    
    var body: some View {
        ScrollView {
            VStack(alignment: .center, spacing: 15) {
                
                HStack {
                    
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "arrow.backward")
                            .foregroundColor(Color("PrimaryColor"))
                            .frame(width: 40, height: 40)
                            .cornerRadius(4)
                            .background(Color("TextFieldColor").cornerRadius(4))
                    })
                    
                    Spacer()
                    
                    Text(enterpriseViewModel.selectedEnterprise?.name ?? "-")
                        .font(.custom("Rubik-Bold", size: 20))
                    
                    Spacer()
                    
                }
                .padding(.horizontal, 16)
                
                VStack(spacing: 24) {
                    
                    HStack {
                        if let enterprisePhotoUrl = enterpriseViewModel.selectedEnterprise?.photo,
                           let imgUrl = URL(string: enterprisePhotoUrl) {
                            URLImage(imgUrl) { image in
                                image
                                    .resizable()
                                    .scaledToFit()
                            }
                        }
                    }
                    
                    Text(enterpriseViewModel.selectedEnterprise?.description ?? "-")
                        .font(.custom("Rubik-Thin", size: 18))
                        .padding(.horizontal, 16)
                    
                }
                
                Spacer()
            }
        }
        .navigationBarHidden(true)
        .onAppear {
            enterpriseViewModel.selectedEnterprise = enterprise
        }
    }
}
