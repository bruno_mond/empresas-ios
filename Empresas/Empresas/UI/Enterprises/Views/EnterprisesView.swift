//
//  EnterprisesView.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import SwiftUI
import URLImage
import URLImageStore

struct EnterprisesView: View {
    
    @ObservedObject var enterpriseViewModel : EnterpriseViewModel = EnterpriseViewModel()
    
    var body: some View {
        
        GeometryReader { geometry in
            
            VStack(spacing: 0) {
                
                header
                    .frame(width: geometry.size.width, height: enterpriseViewModel.isKeyboardShowing ? 90 : 250)
                    .clipped()
                
                searchBar
                    .offset(y: -24)
                    .padding(.bottom, -24)
                    .padding(.horizontal, 16)
                
                if enterpriseViewModel.loading {
                    
                    VStack {
                        
                        Spacer()
                        
                        LoadingView()
                        
                        Spacer()
                    }
                } else if enterpriseViewModel.hasSearch {
                    
                    enterprises
                        .frame(width: geometry.size.width)
                        .ignoresSafeArea(.keyboard, edges: .all)
                    
                }
                
                Spacer()
            }
            
            
        }
        .ignoresSafeArea(.all, edges: .top)
    }
    
    var header : some View {
        ZStack {
            HStack {
                Image("EnterprisesTopBarBackground")
                    .resizable()
                    .scaledToFill()
            }
            
            if !enterpriseViewModel.isKeyboardShowing {
                
                HStack {
                    Image("IoasysLogoSmall")
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFill()
                        .foregroundColor(.white.opacity(0.1))
                        .rotationEffect(.degrees(150))
                }
                .frame(width: 178.5, height: 141)
                .position(x: 370, y: 180)
                
                HStack {
                    Image("IoasysLogoSmall")
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFill()
                        .foregroundColor(.white.opacity(0.1))
                        .rotationEffect(.degrees(180))
                }
                .frame(width: 142.8, height: 112.8)
                .position(x: 110, y: 50)
                
                HStack {
                    Image("IoasysLogoSmall")
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFill()
                        .foregroundColor(.white.opacity(0.1))
                        .rotationEffect(.degrees(50))
                }
                .frame(width: 142.8, height: 112.8)
                .position(x: 370, y: 50)
                
                HStack {
                    Image("IoasysLogoSmall")
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFill()
                        .foregroundColor(.white.opacity(0.1))
                        .rotationEffect(.degrees(320))
                }
                .frame(width: 190.4, height: 150.4)
                .position(x: 240, y: 120)
                
                HStack {
                    Image("IoasysLogoSmall")
                        .renderingMode(.template)
                        .resizable()
                        .scaledToFill()
                        .foregroundColor(.white.opacity(0.1))
                        .rotationEffect(.degrees(110))
                }
                .frame(width: 119, height: 94)
                .position(x: 100, y: 200)
            }
            
        }
    }
    
    
    var searchBar : some View {
        ZStack(alignment: .leading) {
            
            TextField("Pesquise por empresa", text: $enterpriseViewModel.searchString, onCommit: {
                enterpriseViewModel.search()
            })
            .autocapitalization(.sentences)
            .font(.custom("Rubik-Thin", size: 18))
            .padding()
            .padding(.leading, 30)
            .background(RoundedRectangle(cornerRadius: 4).fill(Color("TextFieldColor")))
            .foregroundColor(.black)
            .frame(height: 48)
            
            Image(systemName: "magnifyingglass")
                .foregroundColor(Color("LabelColor"))
                .padding(.horizontal, 14)
        }
    }
    
    var enterprises : some View {
        VStack {
            if enterpriseViewModel.enterprises.count == 0 {
                
                noResult
                
                
            } else {
                
                enterprisesList
                    .padding(.top, 15)
                    .frame(maxWidth: .infinity)
                
            }
        }
        
    }
    
    var noResult : some View {
        VStack {
            Spacer()
            
            Text("Nenhum resultado encontrado")
                .foregroundColor(Color("LabelColor"))
                .font(.custom("Rubik-Thin", size: 18))
            
            Spacer()
        }
    }
    
    var enterprisesList : some View {
        
        VStack(alignment: .leading, spacing: 16) {
            
            HStack {
                
                Text("\(String(format: "%02d", enterpriseViewModel.enterprises.count)) empresas encontradas")
                    .frame(alignment: .leading)
                    .foregroundColor(Color("LabelColor"))
                    .font(.custom("Rubik-Thin", size: 14))
                
                Spacer()
            }
            
            ScrollView(showsIndicators: false) {
                
                VStack(spacing: 8) {
                    ForEach(enterpriseViewModel.enterprises, id: \.id) { enterprise in
                        NavigationLink(
                            destination: EnterpriseDetailView(enterpriseViewModel: enterpriseViewModel, enterprise: enterprise),
                            label: {
                                EnterpriseItemView(enterprise: enterprise)
                            })
                        
                    }
                }
                
            }
        }
        .frame(maxWidth: .infinity)
        .padding(.horizontal, 16)
        
    }
    
}

struct EnterprisesView_Previews: PreviewProvider {
    static var previews: some View {
        EnterprisesView()
    }
}
