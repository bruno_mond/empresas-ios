//
//  EnterpriseItemView.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 19/07/21.
//

import SwiftUI
import APIModule
import URLImage
import URLImageStore

struct EnterpriseItemView: View {
    
    var enterprise : Enterprise
    let urlImageService = URLImageService(fileStore: URLImageFileStore(), inMemoryStore: URLImageInMemoryStore())
    
    var body: some View {
        VStack {
            if let enterprisePhotoUrl = enterprise.photo, let imgUrl = URL(string: enterprisePhotoUrl) {
                URLImage(imgUrl) { image in
                    image
                        .resizable()
                        .scaledToFill()
                }
                .frame(height: 120)
                .cornerRadius(4)
                .clipped()
                .environment(\.urlImageService, urlImageService)
                .overlay(
                    
                    VStack {
                        Spacer()
                        
                        Text(enterprise.name ?? "-")
                            .font(.custom("Rubik-Bold", size: 18))
                            .lineLimit(1)
                            .foregroundColor(.white)
                            .padding(.bottom, 20)
                    }
                    .frame(maxWidth: .infinity)
                    .background(LinearGradient(gradient: Gradient(colors: [Color(UIColor.clear), Color.black]), startPoint: .center, endPoint: .bottom).cornerRadius(4))
                    
                )
            } else {
                Rectangle()
                    .fill(Color("SecondaryColor"))
                    .frame(height: 120)
                    .overlay(
                        Text(enterprise.name ?? "-")
                            .font(.custom("Rubik-Bold", size: 18))
                            .lineLimit(1)
                            .foregroundColor(.white)
                    )
            }
        }
    }
}
