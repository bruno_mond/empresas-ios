//
//  UIApplication+Extensions.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 12/07/21.
//

import SwiftUI

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter{$0.isKeyWindow}
            .first?
            .endEditing(force)
    }
}
