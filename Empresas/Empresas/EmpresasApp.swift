//
//  EmpresasApp.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import SwiftUI
import APIModule

@main
struct EmpresasApp: App {
    @StateObject var authAdapter : AuthRepository = AuthRepository.shared
    
    var body: some Scene {
        WindowGroup {
            if authAdapter.entity == nil {
                LoginView()
            } else {
                NavigationView {
                    EnterprisesView()
                }
            }
        }
    }
}
