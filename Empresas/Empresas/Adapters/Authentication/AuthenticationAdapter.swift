//
//  AuthenticationAdapter.swift
//  Empresas
//
//  Created by Bruno Monteiro de Andrade on 10/07/21.
//

import SwiftUI

class AuthenticationAdapter : ObservableObject {
    
    @AppStorage("authEntity") var entityData : Data?
    
    var entity : AuthenticationAdapterEntity? {
        get {
            guard let data = entityData else { return nil }
            let decoder = JSONDecoder()
            if let entity = try? decoder.decode(AuthenticationAdapterEntity.self, from: data) {
                return entity
            }
            
            return nil
        }
        
        set {
            let encoder = JSONEncoder()
            if let data = try? encoder.encode(newValue) {
                DispatchQueue.main.async {
                    self.entityData = data
                }
            }
        }
    }
}
