//
//  LoginServiceMock.swift
//  APIModuleTests
//
//  Created by Bruno Monteiro de Andrade on 12/07/21.
//

import Foundation
@testable import APIModule

class LoginServiceMock : HTTPLoginProtocol {
    
    func signIn(credential: Credentials, completion: @escaping (Result<AuthenticationData, LoginServices.LoginAPIError>) -> Void) {
        
        guard let urlSuccess = Bundle(for: LoginServiceMock.self).url(forResource: "SuccessfulLoginResponse", withExtension: "json"), let dataSuccess = try? Data(contentsOf: urlSuccess) else {
            completion(.failure(.DataNotFound))
            return
        }
        
        if credential.email == "testeapple@ioasys.com.br" && credential.password == "12341234" {
            guard let response = try? JSONSerialization.jsonObject(with: dataSuccess, options: .allowFragments) as? [String : Any],
                  let headerInfo = response["headerInfo"],
                  let headerData = try? JSONSerialization.data(withJSONObject: headerInfo, options: .fragmentsAllowed) else {
                completion(.failure(.DataNotFound))
                return
            }
            if let authData = try? JSONDecoder().decode(AuthenticationData.self, from: headerData) {
                completion(.success(authData))
            } else {
                completion(.failure(.DataNotFound))
            }
            
        } else {
            completion(.failure(.Unauthorized))
        }
    }
    
    
}
