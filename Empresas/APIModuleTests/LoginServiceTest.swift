//
//  LoginServiceTest.swift
//  APIModuleTests
//
//  Created by Bruno Monteiro de Andrade on 12/07/21.
//

@testable import APIModule
import XCTest

class LoginServiceTest: XCTestCase {

    var service : LoginServiceMock!
    var credentials : Credentials!
    var authHeader : AuthenticationData?
    
    override func setUp() {
        self.service = LoginServiceMock()
        self.credentials = Credentials()
    }
    
    override func tearDown() {
        self.service = nil
        self.credentials = nil
        self.authHeader = nil
    }
    

    
    func test_login_service_success() throws {
        
        
        credentials.email = "testeapple@ioasys.com.br"
        credentials.password = "12341234"
        
        let expectation = self.expectation(description: "AuthEntity")
        var authData : AuthenticationData?
        var error : Error?
        
        func login(credential: Credentials, completion: @escaping (_ success: () throws -> Void) -> Void) {
            service.signIn(credential: credentials) { result in
                switch result {
                
                case .success(let authEntity) :
                    authData = authEntity
                    completion{ return }
                    expectation.fulfill()
                case .failure(let err):
                    completion({ throw err })
                    expectation.fulfill()
                    break
                }
            }
        }
        
        login(credential: credentials) { success in
            do {
                try success()
            } catch let e {
                error = e
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(authData)
        XCTAssertNil(error)
    }
    
    
    func test_login_service_failure() throws {
        
        
        credentials.email = "testeapple@ioasys.com.br"
        credentials.password = "123456"
        
        let expectation = self.expectation(description: "AuthEntity")
        var authData : AuthenticationData?
        var error : Error?
        
        func login(credential: Credentials, completion: @escaping (_ success: () throws -> Void) -> Void) {
            service.signIn(credential: credentials) { result in
                switch result {
                
                case .success(let authEntity) :
                    authData = authEntity
                    completion{ return }
                    expectation.fulfill()
                case .failure(let err):
                    completion({ throw err })
                    expectation.fulfill()
                    break
                }
            }
        }
        
        login(credential: credentials) { success in
            do {
                try success()
            } catch let e {
                error = e
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(authData)
        XCTAssertNotNil(error)
        XCTAssertTrue(error is LoginServices.LoginAPIError)
    }

}
